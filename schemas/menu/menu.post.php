<?php
/****************************************************************************
 *                                                                          *
 *   © ASAP Lab Ltd.                                                        *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

$schema['top']['administration']['items']['cache_monitor_logs'] = [
    'attrs' => [
        'class'=> 'is-addon'
    ],
    'href' => 'cache_monitor_logs.manage',
    'position' => 850
];

return $schema;
